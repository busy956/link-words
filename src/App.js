import './App.css';
import DefaultLayout from "./layouts/DefaultLayout";
import StartGame from "./pages/StartGame";
import WordsGame from "./pages/WordsGame";
import {Route, Routes} from "react-router-dom";

function App() {
    return (
        <div className="App">
            <Routes>
                <Route path="/" element={<DefaultLayout><StartGame/></DefaultLayout>}/>
                |
                <Route path="/WordsGame" element={<DefaultLayout><WordsGame/></DefaultLayout>} />
            </Routes>
        </div>
    );
}

export default App;
