import React from "react";
import {useNavigate} from "react-router-dom";

const StartGame = () => {
    // navigate함수를 호출해서 인자를 전달한다
    const navigate = useNavigate()

    const moveToGame = () => {
        navigate("/WordsGame")
    }

    return (
        <div>
             <h1>끝말잇기 게임</h1>
            <button onClick={moveToGame}>시작</button>
        </div>
    )
}

export default StartGame