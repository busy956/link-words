import React from "react";

const WordsGame = () => {
    // 단어 배열
    const [words, setWords] = React.useState('')
    // 현재 단어
    const [stack, setStack] = React.useState([])
    // 에러 메세지
    const [error, setError] = React.useState('')
    // 게임 끝
    const [gameSet, setGameSet] = React.useState(false)

    // 입력값이 바뀌면 값도 바뀐다
    const handleChanege = (e) => {
        setWords(e.target.value)
    }

    // 초기화
    const handleReStart = () => {
        setWords([])
        setError('')
        setGameSet(false)
    }

    const handleSubmit = (e) => {
        // 이벤트를 막는다
        // form으로 e를 받는데 form안에 이벤트가 여러개 있다(input button)
        // 언급하지 않으면 기본값은 없다
        // 이벤트를 넘겨받았는데 같이 실행되면 안되니까 이벤트를 기본값으로 돌려놓는 것이다
        // form에서 넘겨주면 안에 있는 요소도 같이 넘겨준다(input, button)
        e.preventDefault();
        // 스택에 저장.... 일단 냅다 push
        // 2글자 이상이면서 20글자 미만이면서 마지막 단어가 입력한 단어와 일치하면
        if(words.length >=2 && words.length < 20 && stack[stack.length - 1].word === words) {
            setError('같은 단어는 연속으로 올 수 없습니다')
            // 스택의 길이가 0이거나
        } else if (stack.length === 0 || stack[stack.length - 1].word.slice(-1) === words[0]) {
            if (stack.length < 20) {
                setStack([...stack, { words, id: stack.length + 1 }])
                setWords('')
            }
        } else {
            setError('글자는 2글자 이상 20글자 미만으로 입력해주세요')
        }
    }

    // 올바른 단어제한
    const correctLink = () => {
        if (words) {
            setWords(words)
        } else {
            setError('끝말잇기가 성립되지 않습니다')
        }
    }

    // form 양식으로 안에 넣으면 하나로 묶인다
    // onClick을 써줄 필요가 없다
    return (
        <div>
            <p>단어를 입력하세요</p>
            <br/>
            <form onSubmit={handleSubmit}>
                <input value={words} onChange={handleChanege}/>
                <button type="submit">입력</button>
            </form>
            <br/>
            <button onClick={handleReStart}>다시하기</button>
            <p>{error}</p>
            <p>
                1. 실험용
                2. 시러용
                3. 시럽용
            </p>
        </div>
    )
}

export default WordsGame